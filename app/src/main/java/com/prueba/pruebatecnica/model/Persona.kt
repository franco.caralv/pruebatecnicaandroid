package com.prueba.pruebatecnica.model

data class Persona(
    val __v: Int? = null,
    val _id: String? = null,
    val createdAt: String? = null,
    val persona_comentario: List<PersonaComentario> = emptyList(),
    val persona_content: String,
    val persona_genero: String,
    val persona_puntacion: Float=0f,
    val persona_despuntacion: Float=0f,
    val persona_porcentaje: Float=0f,
    val persona_title: String,
    val updatedAt: String? = null
) {

}