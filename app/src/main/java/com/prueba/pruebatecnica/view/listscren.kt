package com.prueba.pruebatecnica.view

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.FabPosition
import androidx.compose.material.Scaffold
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Mood
import androidx.compose.material.icons.filled.SentimentDissatisfied
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarConfig
import com.gowtham.ratingbar.RatingBarStyle
import com.prueba.pruebatecnica.routing.AppScrean
import com.prueba.pruebatecnica.service.Ciclodevida
import com.prueba.pruebatecnica.viewmodel.listscreenViewmodel
import com.valentinilk.shimmer.shimmer
import kotlinx.coroutines.delay

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun Listscren(controller: NavHostController, viewmodel: listscreenViewmodel = hiltViewModel()) {

    val listSet by viewmodel.listpersonas.observeAsState(initial = emptyList())
    val iscargando by viewmodel.iscargando.observeAsState(initial = false)

    Ciclodevida(onCreate = {
        viewmodel.cargarlist()
    })
    var refreshing by remember { mutableStateOf(false) }
    LaunchedEffect(refreshing) {
        if (refreshing) {
            delay(2000)
            refreshing = false
        }
    }
    Scaffold(
        topBar = {

        },
        floatingActionButton = {
            FloatingActionButton(onClick = {
                controller.navigate(AppScrean.agregar.router)
            }) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = "Crear nota"
                )
            }
        },
        floatingActionButtonPosition = FabPosition.End
    ) {
        SwipeRefresh(
            state = rememberSwipeRefreshState(refreshing),
            onRefresh = { viewmodel.cargarlist() },
        ) {

            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .padding(10.dp),
                verticalArrangement = Arrangement.spacedBy(10.dp),
            ) {

                if (iscargando) {

                    items(15) {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .heightIn(200.dp)
                                .shimmer()
                                .background(Color(192, 192, 192, 255)),
                        ) {

                        }
                    }
                } else {
                    itemsIndexed(listSet) { index, persona ->
                        Card(
                            modifier = Modifier
                                .fillMaxWidth()
                                .heightIn(200.dp)
                                .clickable {
                                    controller.navigate(AppScrean.detalle.router + "/" + persona._id)
                                },
                            elevation = 20.dp,
                            shape = RoundedCornerShape(10.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxHeight()
                                    .fillMaxWidth()
                                    .padding(10.dp),
                                verticalArrangement = Arrangement.SpaceAround
                            ) {
                                Row(
                                    horizontalArrangement = Arrangement.SpaceBetween,
                                    modifier = Modifier.fillMaxWidth()
                                ) {
                                    Text(text = persona.persona_title)
                                    Text(text = persona.persona_genero)
                                }
                                Text(text = persona.persona_content, maxLines = 5)
                                Row(
                                    horizontalArrangement = Arrangement.SpaceBetween,
                                    modifier = Modifier.fillMaxWidth()
                                ) {
                                    RatingBar(
                                        value = persona.persona_porcentaje,
                                        config = RatingBarConfig()
                                            .style(RatingBarStyle.Normal),
                                        onValueChange = {

                                        },
                                        onRatingChanged = {
                                            Log.d("TAG", "onRatingChanged: $it")
                                        }
                                    )
                                    Row(
                                        horizontalArrangement = Arrangement.End
                                    ) {
                                        IconButton(onClick = {
                                            viewmodel.subirpuntacion("${persona._id}",persona)
                                        }) {
                                            Row() {
                                                Text(text = "${persona.persona_puntacion}")
                                                Icon(Icons.Default.Mood, contentDescription = "")
                                            }
                                        }
                                        IconButton(onClick = {
                                            viewmodel.subirpuntacion("${persona._id}",persona,false)
                                        }) {
                                           Row() {
                                               Text(text = "${persona.persona_despuntacion}")
                                               Icon(
                                                   Icons.Default.SentimentDissatisfied,
                                                   contentDescription = ""
                                               )
                                           }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}






