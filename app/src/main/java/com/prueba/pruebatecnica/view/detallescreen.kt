package com.prueba.pruebatecnica.view

import android.util.Log
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mood
import androidx.compose.material.icons.filled.SentimentDissatisfied
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarConfig
import com.gowtham.ratingbar.RatingBarStyle
import com.prueba.pruebatecnica.model.PersonaComentario
import com.prueba.pruebatecnica.service.Ciclodevida
import com.prueba.pruebatecnica.viewmodel.DetalleViewModel

@Composable
fun Detallescreen(
    controller: NavHostController,
    string: String?,
    viewmodel: DetalleViewModel = hiltViewModel()
) {

    val Persona by viewmodel.persona.observeAsState()
    val stringcomentario by viewmodel.textcomentario.observeAsState("")

    Ciclodevida(onCreate = {
        viewmodel.recagar(string!!)
    })
    Persona?.let { persona ->
        Box(
            modifier = Modifier

                .fillMaxWidth()
                .fillMaxHeight()

        ) {

            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.8f)
            ) {
                item {
                    Card(
                        modifier = Modifier
                            .fillMaxWidth()
                            .heightIn(200.dp),
                        elevation = 20.dp,
                        shape = RoundedCornerShape(10.dp)
                    ) {
                        Column(
                            modifier = Modifier
                                .fillMaxHeight()
                                .fillMaxWidth()
                                .padding(10.dp),
                            verticalArrangement = Arrangement.SpaceAround
                        ) {
                            Row(
                                horizontalArrangement = Arrangement.SpaceBetween,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                androidx.compose.material3.Text(text = persona.persona_title)
                                androidx.compose.material3.Text(text = persona.persona_genero)
                            }
                            androidx.compose.material3.Text(
                                text = persona.persona_content,
                                maxLines = 5
                            )
                            Row(
                                horizontalArrangement = Arrangement.SpaceBetween,
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                RatingBar(
                                    value = persona.persona_porcentaje,
                                    config = RatingBarConfig()
                                        .style(RatingBarStyle.Normal),
                                    onValueChange = {

                                    },
                                    onRatingChanged = {
                                        Log.d("TAG", "onRatingChanged: $it")
                                    }
                                )
                                Row(
                                    horizontalArrangement = Arrangement.End
                                ) {
                                    IconButton(onClick = {

                                    }) {
                                        Row() {
                                            androidx.compose.material3.Text(text = "${persona.persona_puntacion}")
                                            Icon(Icons.Default.Mood, contentDescription = "")
                                        }
                                    }
                                    IconButton(onClick = {

                                    }) {
                                        Row() {
                                            androidx.compose.material3.Text(text = "${persona.persona_despuntacion}")
                                            Icon(
                                                Icons.Default.SentimentDissatisfied,
                                                contentDescription = ""
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                item {
                    Text(
                        text = "Comentarios",
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(10.dp),
                        fontSize = 15.sp,
                        fontStyle = FontStyle.Italic
                    )
                }
                itemsIndexed(persona.persona_comentario) { index, item ->
                    Text(
                        text = item.comentario_descripcion,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(10.dp),
                        fontSize = 15.sp,
                        fontStyle = FontStyle.Italic
                    )
                }
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.2f)
                    .align(Alignment.BottomCenter)
            ) {
                OutlinedTextField(value = stringcomentario, onValueChange = {
                    viewmodel.setComentario(it)
                }, modifier = Modifier.fillMaxWidth().padding(horizontal =  10.dp))
                Button(onClick = {
                    viewmodel.createComentarioview(string!!, PersonaComentario(stringcomentario))
                }, modifier = Modifier.fillMaxWidth().padding(horizontal =  10.dp)) {
                    Text(text = "Agregar")
                }
            }

        }
    }

}




