package com.prueba.pruebatecnica.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.prueba.pruebatecnica.viewmodel.AgregarViewModel


@Composable
fun agregar(controller: NavHostController, viewmodel: AgregarViewModel = hiltViewModel()) {

    val titulo by viewmodel.titulo.observeAsState(initial = "")

    val contend by viewmodel.contend.observeAsState(initial = "")

    val genero by viewmodel.genero.observeAsState(initial = "")

    val iscargando by viewmodel.iscargando.observeAsState(initial = false)



    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .padding(10.dp)
    ) {
        OutlinedTextField(value = titulo, label = {
            Text(text = "Titulo")
        }, onValueChange = {
            viewmodel.ingresadodata(it, contend, genero)
        }, modifier = Modifier.fillMaxWidth())
        OutlinedTextField(value = genero, label = {
            Text(text = "Genero")
        }, onValueChange = {
            viewmodel.ingresadodata(titulo, contend, it)
        }, modifier = Modifier.fillMaxWidth())
        OutlinedTextField(value = contend, label = {
            Text(text = "Contenido")
        }, onValueChange = {
            viewmodel.ingresadodata(titulo, it, genero)
        }, modifier = Modifier.fillMaxWidth())

        Button(onClick = {
            viewmodel.guardar(titulo, contend, genero, controller)

        }, modifier = Modifier.fillMaxWidth()) {
            Text(text = "Agregar")
        }
        Button(onClick = {
            controller.popBackStack()
        }, modifier = Modifier.fillMaxWidth()) {
            Text(text = "Regresar")
        }
    }

}


/*
@Preview(showBackground = true)
@Composable
fun agregarPreview() {
    PruebatecnicaTheme {
        agregar()
    }
}
*/
