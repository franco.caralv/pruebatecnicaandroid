package com.prueba.pruebatecnica.di

import com.prueba.pruebatecnica.api.PersonaAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    val BASE_URL="https://movilidad.crediunica.com/"

    @Singleton
    @Provides
    fun providerRetrofield():Retrofit{
       return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(
                OkHttpClient()
                    .newBuilder()
                    .callTimeout(30, TimeUnit.SECONDS)
                    .build()
            )
            .build()
    }

    @Singleton
    @Provides
    fun  providerPersonaApi(retrofit: Retrofit):PersonaAPI{
        return retrofit.create(PersonaAPI::class.java)
    }



}