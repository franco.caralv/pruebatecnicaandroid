package com.prueba.pruebatecnica.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.prueba.pruebatecnica.model.Persona
import com.prueba.pruebatecnica.usecase.ListPersonas
import com.prueba.pruebatecnica.usecase.Updatepersona
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class listscreenViewmodel @Inject constructor(
    private val updatepersona:Updatepersona,
    private val ListarPersonas:ListPersonas
)  : ViewModel() {

    private val _listpersonas: MutableLiveData<List<Persona>> = MutableLiveData()

    val listpersonas: LiveData<List<Persona>> = _listpersonas

    private val _iscargando: MutableLiveData<Boolean> = MutableLiveData()

    val iscargando: LiveData<Boolean> = _iscargando

    fun cargarlist() {
        viewModelScope.launch {
            _iscargando.value = true
            _listpersonas.value = ListarPersonas()
            _iscargando.value = false
        }
    }

    fun subirpuntacion(id: String, persona: Persona, isactivo: Boolean = true) {
        viewModelScope.launch {
            val personacop = if (isactivo) {
                persona.copy(persona_puntacion = persona.persona_puntacion + 1)
            } else {
                persona.copy(persona_despuntacion = persona.persona_despuntacion + 1)
            }
            updatepersona(id, personacop)
            _listpersonas.value = ListarPersonas()
        }
    }


}