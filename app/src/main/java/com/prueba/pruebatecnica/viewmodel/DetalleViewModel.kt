package com.prueba.pruebatecnica.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.prueba.pruebatecnica.model.Persona
import com.prueba.pruebatecnica.model.PersonaComentario
import com.prueba.pruebatecnica.usecase.CreateComentario
import com.prueba.pruebatecnica.usecase.GetPersona
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class DetalleViewModel  @Inject constructor(
    private val getPersona:GetPersona,
    private val createComentario:CreateComentario
)  : ViewModel() {


    private val _persona: MutableLiveData<Persona> = MutableLiveData()

    val persona: LiveData<Persona> = _persona

    private val _textcometario: MutableLiveData<String> = MutableLiveData()
    val textcomentario:LiveData<String> =_textcometario


    fun setComentario(comentario: String){
        _textcometario.value=comentario
    }


    fun recagar(id: String) {
        viewModelScope.launch {
            _persona.value = getPersona(id)
        }
    }


    fun createComentarioview(id: String, comentario: PersonaComentario ) {

        viewModelScope.launch {
            createComentario(id, comentario)?.let { _persona.value = it }
        }

    }


}