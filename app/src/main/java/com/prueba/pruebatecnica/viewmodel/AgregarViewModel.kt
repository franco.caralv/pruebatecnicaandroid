package com.prueba.pruebatecnica.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import com.prueba.pruebatecnica.model.Persona
import com.prueba.pruebatecnica.routing.AppScrean
import com.prueba.pruebatecnica.usecase.CreateUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AgregarViewModel @Inject constructor(
    private val createuser:CreateUser
) : ViewModel() {

    private val _titulo: MutableLiveData<String> = MutableLiveData()
    val titulo: LiveData<String> = _titulo

    private val _contend: MutableLiveData<String> = MutableLiveData()
    val contend: LiveData<String> = _contend

    private val _genero: MutableLiveData<String> = MutableLiveData()
    val genero: LiveData<String> = _genero

    private val _iscargando: MutableLiveData<Boolean> = MutableLiveData()

    val iscargando: LiveData<Boolean> = _iscargando


    fun ingresadodata(titulo: String, contend: String, genero: String) {
        _titulo.value = titulo
        _contend.value = contend
        _genero.value = genero
    }

    fun guardar(titulo: String, contend: String, genero: String, controller: NavHostController) {
        val persom = Persona(
            persona_genero = genero,
            persona_puntacion = 0f,
            persona_title = titulo,
            persona_content = contend,
            persona_comentario = emptyList(),
        )
        viewModelScope.launch {
            _iscargando.value = true
            createuser(persom)
            _iscargando.value = false
            controller.navigate(AppScrean.listscren.router)
        }

    }


}