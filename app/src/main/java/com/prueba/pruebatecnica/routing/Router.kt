package com.prueba.pruebatecnica.routing

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.prueba.pruebatecnica.view.Detallescreen
import com.prueba.pruebatecnica.view.Listscren
import com.prueba.pruebatecnica.view.agregar

@Composable
fun Routeng(){

    val controller = rememberNavController()

    val pantalla: String =AppScrean.listscren.router
    
    NavHost(navController = controller, startDestination = pantalla){
        composable(route = AppScrean.listscren.router){
            Listscren(controller)
        }
        composable(route = AppScrean.detalle.router+ "/{id}", arguments = listOf(navArgument("id"){
            type = NavType.StringType
        })){
            Detallescreen(controller, it.arguments?.getString("id"))
        }
        composable(route =AppScrean.agregar.router){
            agregar(controller)
        }
    }


}