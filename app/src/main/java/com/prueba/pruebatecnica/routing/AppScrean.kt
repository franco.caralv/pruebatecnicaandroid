package com.prueba.pruebatecnica.routing

import androidx.compose.ui.graphics.vector.ImageVector

sealed class AppScrean(val router:String,val titulo:String ,val Icon: ImageVector? =null){

    object listscren:AppScrean("listscren","Bienvenido")
    object detalle:AppScrean("detalle","Detalle")
    object agregar:AppScrean("agregar","Agregar")

}
