package com.prueba.pruebatecnica.usecase

import com.prueba.pruebatecnica.model.Persona
import com.prueba.pruebatecnica.service.PersonaService
import javax.inject.Inject

class Updatepersona @Inject constructor(
    val personaService: PersonaService
) {

    suspend operator fun invoke(id: String, persona: Persona): Persona? {
        val response = personaService.updatepersona(id, persona)
        if (response?.statuscode == 200 && response?.tipo == 1) {
            return response.data
        }
        return null
    }

}