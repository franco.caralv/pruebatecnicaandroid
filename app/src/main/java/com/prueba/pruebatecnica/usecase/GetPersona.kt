package com.prueba.pruebatecnica.usecase

import com.prueba.pruebatecnica.model.Persona
import com.prueba.pruebatecnica.service.PersonaService
import javax.inject.Inject

class GetPersona @Inject constructor(val personaService : PersonaService)  {



    suspend operator fun invoke(id:String): Persona? {
        val response = personaService.getpessona(id)
        if (response?.statuscode == 200 && response?.tipo == 1) {
            return response.data
        }
        return null
    }

}