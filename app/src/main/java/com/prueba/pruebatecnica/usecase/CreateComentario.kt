package com.prueba.pruebatecnica.usecase

import com.prueba.pruebatecnica.model.Persona
import com.prueba.pruebatecnica.model.PersonaComentario
import com.prueba.pruebatecnica.service.PersonaService
import javax.inject.Inject

class CreateComentario @Inject constructor(val personaService : PersonaService) {

    suspend operator fun invoke(id: String, comentario: PersonaComentario): Persona? {
        val response = personaService.createcomentario(id,comentario)
        if (response?.statuscode == 200 && response?.tipo == 1) {
            return response.data
        }
        return null
    }

}