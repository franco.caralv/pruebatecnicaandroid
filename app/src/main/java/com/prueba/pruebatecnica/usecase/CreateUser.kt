package com.prueba.pruebatecnica.usecase

import com.prueba.pruebatecnica.model.Persona
import com.prueba.pruebatecnica.service.PersonaService
import javax.inject.Inject

class CreateUser @Inject constructor(val personaService : PersonaService)  {

    suspend operator fun invoke(persona: Persona): Persona? {
        val response = personaService.createuser(persona)
        if (response?.statuscode == 200 && response?.tipo == 1) {
            return response.data
        }
        return null
    }

}