package com.prueba.pruebatecnica.usecase

import com.prueba.pruebatecnica.model.Persona
import com.prueba.pruebatecnica.service.PersonaService
import javax.inject.Inject

class ListPersonas @Inject constructor(val personaService : PersonaService)  {

    suspend operator fun invoke(): List<Persona> {

        val response = personaService.getlispersona()
        if (response?.statuscode == 200 && response?.tipo == 1) {
            return response.data
        }
        return emptyList()
    }

}