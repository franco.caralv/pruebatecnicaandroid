package com.prueba.pruebatecnica.api

import com.prueba.pruebatecnica.model.Persona
import com.prueba.pruebatecnica.model.PersonaComentario
import com.prueba.pruebatecnica.model.Responsev1
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Url

interface PersonaAPI {

    @POST
    suspend fun  createpersona(@Url url: String, @Body persona: Persona): Response<Responsev1<Persona>>

    @GET
    suspend fun getlistpersonas(@Url url:String):Response<Responsev1<List<Persona>>>

    @POST
    suspend fun createcomentario(@Url url: String, @Body persona: PersonaComentario): Response<Responsev1<Persona>>

    @GET
    suspend fun  getpersona(@Url url: String): Response<Responsev1<Persona>>

    @PATCH
    suspend fun updatepersona(@Url url: String, @Body persona: Persona): Response<Responsev1<Persona>>


}