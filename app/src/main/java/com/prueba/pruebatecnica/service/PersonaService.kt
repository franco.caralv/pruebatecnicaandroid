package com.prueba.pruebatecnica.service

import android.util.Log
import com.prueba.pruebatecnica.api.PersonaAPI
import com.prueba.pruebatecnica.model.Persona
import com.prueba.pruebatecnica.model.PersonaComentario
import com.prueba.pruebatecnica.model.Responsev1
import javax.inject.Inject

class PersonaService @Inject constructor(val personaAPI: PersonaAPI) {

    suspend fun getlispersona(): Responsev1<List<Persona>>? {
        var body: Responsev1<List<Persona>>? = null
        try {
            val response =
                personaAPI.getlistpersonas("persona")
            body = response.body()
        } catch (e: java.lang.Exception) {
            Log.e("error", e.message + "")
        }
        return body
    }

    suspend fun getpessona(id: String): Responsev1<Persona>? {
        var body: Responsev1<Persona>? = null
        try {
            val response =
                personaAPI.getpersona("persona/${id}")
            body = response.body()
        } catch (e: java.lang.Exception) {
            Log.e("error", e.message + "")
        }
        return body
    }

    suspend fun createuser(persona: Persona): Responsev1<Persona>? {
        var body: Responsev1<Persona>? = null
        try {
            val response =
                personaAPI
                    .createpersona("persona", persona)
            body = response.body()
        } catch (e: java.lang.Exception) {
            Log.e("error", e.message + "")
        }
        return body
    }

    suspend fun createcomentario(id: String, comentario: PersonaComentario): Responsev1<Persona>? {
        var body: Responsev1<Persona>? = null
        try {
            val response =
                personaAPI
                    .createcomentario("persona/comentario/${id}", comentario)
            body = response.body()
        } catch (e: java.lang.Exception) {
            Log.e("error", e.message + "")
        }
        return body
    }



    suspend fun updatepersona(id: String,persona: Persona): Responsev1<Persona>? {
        var body: Responsev1<Persona>? = null
        try {
            val response =
                personaAPI
                    .updatepersona("persona/${id}", persona)
            body = response.body()
        } catch (e: java.lang.Exception) {
            Log.e("error", e.message + "")
        }
        return body
    }

}