package com.prueba.pruebatecnica.service

import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner

@Composable
fun Ciclodevida(
    lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current, onCreate: () -> Unit = {

    }, onDestroy: () -> Unit = {

    }, onPause: () -> Unit = {

    }, onResume: () -> Unit = {

    },
    onStop:()->Unit={

    },
    onStart:()->Unit={

    }
) = DisposableEffect(lifecycleOwner) {
    val observer = LifecycleEventObserver { _, event ->
        when (event) {
            Lifecycle.Event.ON_CREATE -> {
                onCreate()
            }
            Lifecycle.Event.ON_DESTROY -> {
                onDestroy()
            }
            Lifecycle.Event.ON_PAUSE -> {
                onPause()
            }
            Lifecycle.Event.ON_RESUME -> {
                onResume()
            }
            Lifecycle.Event.ON_START->{
                onStart()
            }
            Lifecycle.Event.ON_STOP->{
                onStop()
            }
            else -> {

            }
        }
    }
    lifecycleOwner.lifecycle.addObserver(observer)
    onDispose {
        lifecycleOwner.lifecycle.removeObserver(observer)
    }
}